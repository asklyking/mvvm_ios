//
//  HomeViewController.swift
//  MVVMRxSwiftIOS
//
//  Created by Sang Duc Tran on 4/25/17.
//  Copyright © 2017 EastgaSang Duc Tran. All rights reserved.
//

import UIKit
import RxSwift

class HomeViewController: UIViewController,UITableViewDelegate {

    var arr_persons : [PersonViewModel] = {
        let person1 = Person.init(first_name: "sang", last_name: "tran", height: 1.54,weight: 50, job: "student")
        let person2 = Person.init(first_name: "anh", last_name: "mai", height: 1.76,weight: 80, job: "student")
        let person3 = Person.init(first_name: "chuong", last_name: "phung", height: 1.80,weight: 70, job: "job")
        let person4 = Person.init(first_name: "son", last_name: "nguyen", height: 1.72,weight: 64, job: "job")
        return [PersonViewModel(person: person1),PersonViewModel(person: person2),PersonViewModel(person: person3),PersonViewModel(person: person4)]
    }()
    var persons : Variable<[PersonViewModel]>!
    
    let PersonCellIdentifier = "PersonCell"
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.persons = Variable(self.arr_persons)
        
        self.tableView.rx_setDelegate(self).addDisposableTo(disposeBag)
        
        self.tableView.registerNib(UINib(nibName: "PersonTableViewCell",bundle: nil), forCellReuseIdentifier: PersonCellIdentifier)
        persons.asObservable().bindTo(tableView.rx_itemsWithCellIdentifier(PersonCellIdentifier, cellType: PersonTableViewCell.self)) { (index, personViewModel: PersonViewModel, cell) in
            cell.updatePersonCellContent(personViewModel)
            }.addDisposableTo(disposeBag)
        self.tableView.rx_itemSelected.subscribeNext{ (indexPath) in
            let personDetailViewController = PersonDetailViewController(nibName: "PersonDetailViewController", bundle: nil)
            personDetailViewController.personViewModel = self.persons.value[indexPath.row]
            self.presentViewController(personDetailViewController, animated: true, completion: nil)
            self.tableView.deselectRowAtIndexPath(indexPath, animated: false)
            }.addDisposableTo(disposeBag)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
