//
//  PersonTableViewCell.swift
//  MVVMRxSwiftIOS
//
//  Created by Sang Duc Tran on 4/25/17.
//  Copyright © 2017 EastgaSang Duc Tran. All rights reserved.
//

import UIKit
import RxSwift

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblIBM: UILabel!
    @IBOutlet weak var lblJob: UILabel!
    
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updatePersonCellContent(personViewModel: PersonViewModel){
        personViewModel.firstnameText.bindTo(lblFirstName.rx_text).addDisposableTo(disposeBag)
        personViewModel.lastnameText.bindTo(lblLastName.rx_text).addDisposableTo(disposeBag)
        
        personViewModel.IBMText.bindTo(lblIBM.rx_text).addDisposableTo(disposeBag)
        
        personViewModel.jobText.bindTo(lblJob.rx_text).addDisposableTo(disposeBag)
    }
    
}
