//
//  PersonDetailViewController.swift
//  MVVMRxSwiftIOS
//
//  Created by Sang Duc Tran on 4/25/17.
//  Copyright © 2017 EastgaSang Duc Tran. All rights reserved.
//

import UIKit
import RxSwift

class PersonDetailViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    var personViewModel : PersonViewModel!

    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfHeight: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tfJob: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.personViewModel.firstnameText.bindTo(self.tfFirstName.rx_text).addDisposableTo(self.disposeBag)
        self.personViewModel.lastnameText.bindTo(self.tfLastName.rx_text).addDisposableTo(self.disposeBag)
        self.personViewModel.heightText.map{String($0)}.bindTo(self.tfHeight.rx_text).addDisposableTo(self.disposeBag)
        self.personViewModel.weightText.map{String($0)}.bindTo(self.tfWeight.rx_text).addDisposableTo(self.disposeBag)
        self.personViewModel.jobText.bindTo(self.tfJob.rx_text).addDisposableTo(self.disposeBag)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func saveData(sender: AnyObject) {
        self.tfFirstName.rx_text.bindTo(self.personViewModel.firstnameText).addDisposableTo(self.disposeBag)
        self.tfLastName.rx_text.bindTo(self.personViewModel.lastnameText).addDisposableTo(self.disposeBag)
        self.tfHeight.rx_text.map{Float($0)!}.bindTo(self.personViewModel.heightText).addDisposableTo(self.disposeBag)
        self.tfWeight.rx_text.map{Float($0)!}.bindTo(self.personViewModel.weightText).addDisposableTo(self.disposeBag)
        self.tfJob.rx_text.bindTo(self.personViewModel.jobText).addDisposableTo(self.disposeBag)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
