//
//  PersonViewModel.swift
//  MVVMRxSwiftIOS
//
//  Created by Sang Duc Tran on 4/25/17.
//  Copyright © 2017 EastgaSang Duc Tran. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PersonViewModel {
    let person: Person
    var disposeBag = DisposeBag()
    
    
    var firstnameText : BehaviorSubject<String>
    var lastnameText : BehaviorSubject<String>
    var heightText : BehaviorSubject<Float>
    var weightText : BehaviorSubject<Float>
    var IBMText : BehaviorSubject<String>
    var jobText : BehaviorSubject<String>
    
    
    
    init(person: Person){
        self.person = person
        firstnameText = BehaviorSubject<String>(value: person.first_name)
        lastnameText = BehaviorSubject<String>(value: person.last_name)
        heightText = BehaviorSubject<Float>(value: person.height)
        weightText = BehaviorSubject<Float>(value: person.weight)
        jobText = BehaviorSubject<String>(value: person.job)
        IBMText = BehaviorSubject(value: "0")
        
        firstnameText.subscribeNext { (firstname) in
            person.first_name = firstname
            }.addDisposableTo(disposeBag)
        lastnameText.subscribeNext { (lastname) in
            person.last_name = lastname
            }.addDisposableTo(disposeBag)
        heightText.subscribeNext { (height) in
            person.height = height
            }.addDisposableTo(disposeBag)
        weightText.subscribeNext { (weight) in
            person.weight = weight
            }.addDisposableTo(disposeBag)
        
        [heightText, weightText].combineLatest { (heathInfo) -> String in
            return "\(heathInfo[1]/heathInfo[0]/heathInfo[0])"
            }.bindTo(IBMText).addDisposableTo(disposeBag)
        
        jobText.subscribeNext { (job) in
            person.job = job
            }.addDisposableTo(disposeBag)
    }
}
