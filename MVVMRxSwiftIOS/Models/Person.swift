//
//  Person.swift
//  MVVMRxSwiftIOS
//
//  Created by Sang Duc Tran on 4/25/17.
//  Copyright © 2017 EastgaSang Duc Tran. All rights reserved.
//

import Foundation
import RxSwift

class Person {
    var first_name: String
    var last_name: String
    var height : Float
    var weight : Float
    var job: String
    
    init(first_name: String,last_name: String, height: Float,weight : Float, job: String){
        self.first_name = first_name
        self.last_name = last_name
        self.height = height
        self.weight = weight
        self.job = job
    }
}
